==========================================
Repositorio de WebApp para la prueba tecnica de Deltec
==========================================

Este es el repositorio para la aplicación de prueba para la gestion de inventario

Desarrollado por: Daniel Lopez


Instrucciones de instalación:
=============================

Clonar el repositorio. 

Crear una base de datos en PostgreSQL con las credenciales especificadas en el settings.

Crear un virtualenv, para mayor comodidad se recomienda utilizar virtualenvwrapper.

Instalar los requirements en el virtualenv::

    pip install -r requirements.txt

Crear las migraciones::

    python manage.py makemigrations

Correr las migraciones::

    python manage.py migrate

Correr el script que crea el usuario principal::

    python manage.py shell < datos_iniciales/crear_usuario_principal.py

Correr los test para verificar que todo esté correcto::

    python manage.py flake8
    
    python manage.py test

Correr el servidor::

    python manage.py runserver

Procedimiento necesario para medir la cobertura de los test:
============================================================

Se debe tener el coverage instalado en el ambiente virtual, una vez en la carpeta del proyecto se debe correr el
siguiente comando con el ambiente activo::

    coverage run --include='./*' manage.py test

Para ver los resultados por la consola se debe usar el siguiente comando::

    coverage report

Para generar los archivos html del coverage se debe usar el siguiente comando::

    coverage html

Paquetes o herramientas utilizadas en el proyecto:
==================================================

- django-bootstrap3: https://github.com/dyve/django-bootstrap3
- coverage: https://bitbucket.org/ned/coveragepy/overview
