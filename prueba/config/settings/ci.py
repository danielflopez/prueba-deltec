from .base import *
from .installed_apps import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['45.58.40.92', '127.0.0.1']

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'circle_test',
        'USER': 'ubuntu',
        'PASSWORD': ''
    },
    'db_logs': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'logs.db',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DEFAULT_FROM_EMAIL = "postmaster@radyconsultores.com"

NOCAPTCHA = True
RECAPTCHA_USE_SSL = False
