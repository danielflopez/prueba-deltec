INSTALLED_APPS = [
    'bootstrap3',
    'datetimewidget',
    'menu_generator',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'usuarios',
    'recursos'
]
