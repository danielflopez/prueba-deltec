from .base import *
from .installed_apps import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_secret("DATABASE_NAME_SERVER_TEST"),
        'USER': get_secret("DATABASE_USER_SERVER_TEST"),
        'PASSWORD': get_secret("DATABASE_PASS_SERVER_TEST"),
        'HOST': get_secret("DATABASE_HOST_SERVER_TEST"),
    }
}

EMAIL_BACKEND = EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
DEFAULT_FROM_EMAIL = "daniel.f.lopez.j@gmail.com"
