from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='base.html'), name='inicio'),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^usuarios/', include('usuarios.urls', namespace='usuarios')),
    url(r'^recursos/', include('recursos.urls', namespace='recursos')),
    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
