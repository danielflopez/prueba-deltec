from django import forms

from usuarios.models import PerfilUsuario
from .models import Recurso, RecursoAsignado


class RecursoForm(forms.ModelForm):

    class Meta:
        model = Recurso
        fields = ('nombre', 'categoria', 'marca', 'serie', 'cantidad')


class RecursoAsignadoForm(forms.ModelForm):

    def __init__(self, usuario, recurso, *args, **kwargs):
        super(RecursoAsignadoForm, self).__init__(*args, **kwargs)
        self.recurso = recurso
        self.fields['recurso'].initial = recurso
        self.fields['recurso'].widget.attrs['readonly'] = True
        self.fields['usuario'].queryset = PerfilUsuario.objects.get_demas_usuarios(usuario)

    def clean_cantidad(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para verificar que el almacen tiene suficiente cantidad de un recurso para asignar un usuario
        :return: cantidad a transferir si tiene suficiente, error de lo contrario
        """
        recurso = self.cleaned_data['recurso']
        cantidad = self.cleaned_data['cantidad']

        total = recurso.cantidad - cantidad

        if total < 0:
            self.add_error('cantidad', 'No hay suficientes recursos')

        return cantidad

    class Meta:
        model = RecursoAsignado
        fields = ('recurso',  'usuario', 'cantidad')


class TransferenciaRecursosForm(forms.ModelForm):

    def __init__(self, usuario, asignacion, *args, **kwargs):
        super(TransferenciaRecursosForm, self).__init__(*args, **kwargs)
        self.asignacion = asignacion
        self.fields['recurso'].initial = asignacion.recurso
        self.fields['recurso'].widget.attrs['readonly'] = True
        self.fields['usuario'].queryset = PerfilUsuario.objects.get_demas_usuarios(usuario)

    def clean_cantidad(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para verificar que el usuario tiene suficiente cantidad de un recurso para asignar a otro
        :return: cantidad a transferir si tiene suficiente, error de lo contrario
        """

        cantidad = self.cleaned_data['cantidad']
        resta = self.asignacion.cantidad - cantidad
        if resta < 0:
            self.add_error('cantidad', 'No tiene suficiente cantidad del recurso para hacer la transferencia')
        return cantidad

    class Meta:
        model = RecursoAsignado
        fields = ('recurso',  'usuario', 'cantidad')
