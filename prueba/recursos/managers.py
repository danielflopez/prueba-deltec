from django.db import models


class RecursoAsignadoManager(models.Manager):

    def get_asignaciones_user(self, usuario):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        :param usuario: usuario al que se le traeran las asignaciones
        :return: QuerySet Filtrado
        """
        return self.filter(usuario=usuario)

    def get_recurso_si_existe_por_usuario(self, recurso, usuario):
        """
        Autor: Daniel Lopez
        Fecha: 26 Agosto 2017
        Método para obtener una asignación de un recurso para un usuario
        :param recurso: instancia de un recurso
        :param usuario: instancia de un usuario
        :return: el objeto si existe, None de lo contrario
        """

        asignacion = self.filter(usuario=usuario, recurso=recurso)

        if asignacion:
            return asignacion.get()
        else:
            return self.none()
