MENUS = {
    "NAV_MENU_LEFT": [
        {
            "name": "Recursos del Inventario",
            "url": "recursos:listar",
            "icon": "fa fa-dropbox",
            "validators": [
                'menu_generator.validators.is_authenticated',
            ]
        }, {
            "name": "Historial de transferencias",
            "url": "recursos:historial_asignaciones",
            "icon": "fa fa-history",
            "validators": [
                'menu_generator.validators.is_authenticated',
            ]
        }, {
            "name": "Mis recursos",
            "url": "recursos:mis_recursos",
            "icon": "fa fa-table",
            "validators": [
                'menu_generator.validators.is_authenticated',
            ]
        },
    ]
}
