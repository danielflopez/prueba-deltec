from django.core.validators import MinValueValidator
from django.db import models
from django.urls import reverse

from .managers import RecursoAsignadoManager
from usuarios.models import PerfilUsuario


class Recurso(models.Model):

    nombre = models.CharField(max_length=50)
    categoria = models.CharField(max_length=50)
    marca = models.CharField(max_length=50)
    serie = models.CharField(max_length=50)
    cantidad = models.IntegerField(validators=[MinValueValidator(0)])

    def restar_cantidad(self, cantidad):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para restar cantidades a un recurso
        :param cantidad: cantidad a restar
        """
        self.cantidad -= cantidad
        self.save()

    def get_absolute_url(self):
        return reverse("recursos:listar")

    def __str__(self):
        return self.nombre


class RecursoAsignado(models.Model):

    recurso = models.ForeignKey(Recurso)
    usuario = models.ForeignKey(PerfilUsuario)
    cantidad = models.IntegerField()
    fecha = models.DateTimeField(auto_now=True)

    objects = models.Manager()
    custom_objects = RecursoAsignadoManager()

    def sumar_cantidad_recurso(self, cantidad):
        """
        Autor: Daniel Lopez
        Fecha: 26 Agosto 2017
        Método para sumar una cantidad a una asignación
        :param cantidad: cantidad a sumar
        """
        self.cantidad += cantidad
        self.save()

    def restar_cantidad_recurso(self, cantidad):
        """
        Autor: Daniel Lopez
        Fecha: 26 Agosto 2017
        Método para restar una cantidad a una asignación
        :param cantidad: cantidad a restar
        """
        self.cantidad -= cantidad
        self.save()

    def get_absolute_url(self):
        return reverse("recursos:historial_asignaciones")

    def __str__(self):
        return "Se le asigno {} de {} a {}".format(self.cantidad, self.recurso, self.usuario)


class HistorialRecursos(models.Model):

    recurso = models.ForeignKey(Recurso)
    fuente = models.ForeignKey(PerfilUsuario, related_name="Fuente", null=True)
    remitente = models.ForeignKey(PerfilUsuario, related_name='remitente')
    destinatario = models.ForeignKey(PerfilUsuario, related_name='destinatario')
    cantidad = models.IntegerField()
    fecha = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Se trasladaron ({}) {} el dia {}, desde {} a {} ".format(self.cantidad, self.recurso, self.fecha,
                                                                         self.remitente, self.destinatario)
