from django.test import TestCase

from usuarios.models import PerfilUsuario


class BaseMixinTest(TestCase):
    """
    Autor: Daniel Lopez
    Fecha: 4 3 2017
    TestCase Base para los mixin de la aplicación users
    """

    def setUp(self):
        super(BaseMixinTest, self).setUp()
        self.usuario = PerfilUsuario.objects.create_user(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario",
            password="prueba"
        )

        self.usuario2 = PerfilUsuario.objects.create_user(
            email='usuario2@usuario.co',
            is_active=True,
            first_name="usuario2",
            last_name="usuario2",
            password="prueba"
        )

    def tearDown(self):
        super(BaseMixinTest, self).tearDown()
        del self.usuario
        del self.usuario2
