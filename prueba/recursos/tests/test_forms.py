from django.test import TestCase

from recursos.forms import RecursoForm, RecursoAsignadoForm, TransferenciaRecursosForm
from recursos.models import Recurso, RecursoAsignado
from usuarios.models import PerfilUsuario


class TestRecursoForm(TestCase):

    def test_blank_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos en blanco en el formulario
        """
        blank_form = RecursoForm(data={})
        self.assertFalse(blank_form.is_valid())
        errors = {
            "nombre": ["Este campo es obligatorio."],
            "categoria": ["Este campo es obligatorio."],
            "marca": ["Este campo es obligatorio."],
            "serie": ["Este campo es obligatorio."],
            "cantidad": ["Este campo es obligatorio."],
        }
        self.assertEqual(blank_form.errors, errors)

    def test_invalid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos no validos del formulario
        """
        invalid_form = RecursoForm(
            data={
                'nombre': 'nombre',
                'categoria': 'categoria',
                'marca': 'marca',
                'serie': 'serie',
                'cantidad': -1
            }
        )
        self.assertFalse(invalid_form.is_valid())
        errors = {
            "cantidad": ["Asegúrese de que este valor es mayor o igual a 0."],
        }
        self.assertEqual(invalid_form.errors, errors)

    def test_valid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: Abril 4 2017
        Método para probar los datos validos del formulario
        """
        valid_form = RecursoForm(
            data={
                'nombre': 'nombre',
                'categoria': 'categoria',
                'marca': 'marca',
                'serie': 'serie',
                'cantidad': 10
            }
        )

        self.assertTrue(valid_form.is_valid())


class TestRecursoAsignadoForm(TestCase):

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

        self.usuario2 = PerfilUsuario.objects.create(
            email='usuario2@usuario.co',
            is_active=True,
            first_name="usuario2",
            last_name="usuario2"
        )

    def tearDown(self):
        del self.recurso
        del self.usuario
        del self.usuario2

    def test_blank_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos en blanco en el formulario
        """
        blank_form = RecursoAsignadoForm(usuario=self.usuario, recurso=self.recurso, data={})
        self.assertFalse(blank_form.is_valid())
        errors = {
            "recurso": ["Este campo es obligatorio."],
            "usuario": ["Este campo es obligatorio."],
            "cantidad": ["Este campo es obligatorio."],
        }
        self.assertEqual(blank_form.errors, errors)

    def test_invalid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos no validos del formulario
        """
        invalid_form = RecursoAsignadoForm(usuario=self.usuario, recurso=self.recurso,
                                           data={
                                               'recurso': self.recurso.id,
                                               'usuario': self.usuario.id,
                                               'cantidad': 100
                                           })
        self.assertFalse(invalid_form.is_valid())
        errors = {
            "usuario": ["Escoja una opción válida. Esa opción no está entre las disponibles."],
            "cantidad": ["No hay suficientes recursos"],
        }
        self.assertEqual(invalid_form.errors, errors)

    def test_valid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: Abril 4 2017
        Método para probar los datos validos del formulario
        """
        valid_form = RecursoAsignadoForm(usuario=self.usuario, recurso=self.recurso,
                                         data={
                                             'recurso': self.recurso.id,
                                             'usuario': self.usuario2.id,
                                             'cantidad': 1
                                         })

        self.assertTrue(valid_form.is_valid())


class TestTransferenciaRecursosForm(TestCase):

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

        self.usuario2 = PerfilUsuario.objects.create(
            email='usuario2@usuario.co',
            is_active=True,
            first_name="usuario2",
            last_name="usuario2"
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario,
            cantidad=3
        )

    def tearDown(self):
        del self.recurso
        del self.usuario
        del self.usuario2
        del self.asignado

    def test_blank_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos en blanco en el formulario
        """
        blank_form = TransferenciaRecursosForm(usuario=self.usuario, asignacion=self.asignado, data={})
        self.assertFalse(blank_form.is_valid())
        errors = {
            "recurso": ["Este campo es obligatorio."],
            "usuario": ["Este campo es obligatorio."],
            "cantidad": ["Este campo es obligatorio."],
        }
        self.assertEqual(blank_form.errors, errors)

    def test_invalid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos no validos del formulario
        """
        invalid_form = TransferenciaRecursosForm(usuario=self.usuario, asignacion=self.asignado,
                                                 data={
                                                     'recurso': self.recurso.id,
                                                     'usuario': self.usuario.id,
                                                     'cantidad': 100
                                                 })

        self.assertFalse(invalid_form.is_valid())
        errors = {
            "usuario": ["Escoja una opción válida. Esa opción no está entre las disponibles."],
            "cantidad": ["No tiene suficiente cantidad del recurso para hacer la transferencia"],
        }
        self.assertEqual(invalid_form.errors, errors)

    def test_valid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: Abril 4 2017
        Método para probar los datos validos del formulario
        """
        valid_form = TransferenciaRecursosForm(usuario=self.usuario, asignacion=self.asignado,
                                               data={
                                                   'recurso': self.recurso.id,
                                                   'usuario': self.usuario2.id,
                                                   'cantidad': 1
                                               })

        self.assertTrue(valid_form.is_valid())
