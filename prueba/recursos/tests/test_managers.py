from django.test import TestCase

from recursos.models import Recurso, HistorialRecursos, RecursoAsignado
from usuarios.models import PerfilUsuario


class TestRecursoAsignadoManager(TestCase):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    TestCase para probar el manager RecursoAsignadoManager
    """

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

        self.usuario2 = PerfilUsuario.objects.create(
            email='usuario2@usuario.co',
            is_active=True,
            first_name="usuario2",
            last_name="usuario2"
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario,
            cantidad=3
        )

        self.historial = HistorialRecursos.objects.create(
            recurso=self.recurso,
            remitente=self.usuario,
            destinatario=self.usuario,
            cantidad=3
        )

    def tearDown(self):
        del self.recurso
        del self.asignado
        del self.usuario
        del self.usuario2
        del self.historial

    def test_get_asignaciones_user(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el metodo get_asignaciones_user
        :return: QuerySet filtrado
        """
        self.assertEqual(list(RecursoAsignado.custom_objects.get_asignaciones_user(self.usuario)),
                         list(RecursoAsignado.objects.filter(usuario=self.usuario)))

    def test_get_recurso_si_existe_por_usuario(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el metodo get_recurso_si_existe_por_usuario
        :return: QuerySet filtrado
        """
        self.assertEqual(
            RecursoAsignado.custom_objects.get_recurso_si_existe_por_usuario(self.recurso, self.usuario),
            RecursoAsignado.objects.filter(usuario=self.usuario, recurso=self.recurso).get())

        self.assertEqual(
            list(RecursoAsignado.custom_objects.get_recurso_si_existe_por_usuario(self.recurso, self.usuario2)),
            list(RecursoAsignado.objects.none()))
