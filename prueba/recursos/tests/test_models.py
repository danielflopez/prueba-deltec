from django.test import TestCase
from django.urls import reverse

from recursos.models import Recurso, RecursoAsignado, HistorialRecursos
from usuarios.models import PerfilUsuario


class TestRecurso(TestCase):
    """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Pruebas para el modelo de Recurso
        """

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

    def tearDown(self):
        del self.recurso

    def test_str_representation_recurso(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar la representación como string del Recurso
        """
        self.assertEqual(str(self.recurso), self.recurso.nombre)

    def test_restar_cantidad(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar el método restar_cantidad
        """
        self.recurso.restar_cantidad(2)
        self.assertEqual(self.recurso.cantidad, 8)


class TestRecursoAsignado(TestCase):

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario,
            cantidad=3
        )

    def tearDown(self):
        del self.recurso
        del self.asignado
        del self.usuario

    def test_str_representation_recursoasignado(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar la representación como string del RecursoAsignado
        """
        self.assertEqual(str(self.asignado),
                         "Se le asigno {} de {} a {}".format(self.asignado.cantidad,
                                                             self.asignado.recurso,
                                                             self.asignado.usuario))

    def test_sumar_cantidad_recurso(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar el método sumar_cantidad_recurso
        """
        self.asignado.sumar_cantidad_recurso(2)
        self.assertEqual(self.asignado.cantidad, 5)

    def test_restar_cantidad_recurso(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar el método restar_cantidad_recurso
        """
        self.asignado.restar_cantidad_recurso(2)
        self.assertEqual(self.asignado.cantidad, 1)


class TestHistorialRecursos(TestCase):

    def setUp(self):
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario,
            cantidad=3
        )

        self.historial = HistorialRecursos.objects.create(
            recurso=self.recurso,
            remitente=self.usuario,
            destinatario=self.usuario,
            cantidad=3
        )

    def tearDown(self):
        del self.recurso
        del self.asignado
        del self.usuario
        del self.historial

    def test_str_representation_recursoasignado(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar la representación como string del HistorialRecursos
        """
        self.assertEqual(str(self.historial),
                         "Se trasladaron ({}) {} el dia {}, desde {} a {} ".format(
                             self.historial.cantidad,
                             self.historial.recurso,
                             self.historial.fecha,
                             self.historial.remitente,
                             self.historial.destinatario))

    def test_get_absolute_url(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el método get_
        """
        self.assertEqual(self.asignado.get_absolute_url(), reverse("recursos:historial_asignaciones"))
