from django.core.urlresolvers import reverse

from . import BaseMixinTest
from recursos.models import Recurso, RecursoAsignado


class TestRecursoCreateView(BaseMixinTest):

    def setUp(self):
        super(TestRecursoCreateView, self).setUp()
        self.url = reverse('recursos:crear')

    def tearDown(self):
        super(TestRecursoCreateView, self).tearDown()
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de crear recurso
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar la creacion de un recurso
        """
        data = {
            'nombre': 'nombre',
            'categoria': 'categoria',
            'marca': 'marca',
            'serie': 'serie',
            'cantidad': 10
        }

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


class TestRecursoList(BaseMixinTest):

    def setUp(self):
        super(TestRecursoList, self).setUp()
        self.url = reverse('recursos:listar')

    def tearDown(self):
        super(TestRecursoList, self).tearDown()
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de listar recursos
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestMisRecursoList(BaseMixinTest):

    def setUp(self):
        super(TestMisRecursoList, self).setUp()
        self.url = reverse('recursos:mis_recursos')

    def tearDown(self):
        super(TestMisRecursoList, self).tearDown()
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de listar mis recursos
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class TestRecursoUpdateView(BaseMixinTest):

    def setUp(self):
        super(TestRecursoUpdateView, self).setUp()
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )
        self.url = reverse('recursos:editar', kwargs={'pk': self.recurso.id})

    def tearDown(self):
        super(TestRecursoUpdateView, self).tearDown()
        del self.recurso
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de editar recurso
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar la edicion de un recurso
        """
        data = {
            'nombre': 'nombre',
        }

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)


class TestRecursoAsignadoCreateView(BaseMixinTest):

    def setUp(self):
        super(TestRecursoAsignadoCreateView, self).setUp()
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.recurso2 = Recurso.objects.create(
            nombre='nombre2',
            categoria='categoria2',
            marca="marca2",
            serie="0012",
            cantidad=10
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario2,
            cantidad=3
        )

        self.url = reverse('recursos:crear_asignacion', kwargs={'pk': self.recurso.id})

    def tearDown(self):
        super(TestRecursoAsignadoCreateView, self).tearDown()
        del self.recurso
        del self.recurso2
        del self.asignado
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de crear asignación
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar la redirección despues de crear asignación
        """

        data = {
            'recurso': self.recurso.id,
            'usuario': self.usuario2.id,
            'cantidad': 1
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)

        data = {
            'recurso': self.recurso2.id,
            'usuario': self.usuario2.id,
            'cantidad': 1
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


class TestTransferirRecursoFormView(BaseMixinTest):

    def setUp(self):
        super(TestTransferirRecursoFormView, self).setUp()
        self.recurso = Recurso.objects.create(
            nombre='nombre',
            categoria='categoria',
            marca="marca",
            serie="001",
            cantidad=10
        )

        self.recurso2 = Recurso.objects.create(
            nombre='nombre2',
            categoria='categoria2',
            marca="marca2",
            serie="0012",
            cantidad=10
        )

        self.asignado = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario2,
            cantidad=3
        )

        self.asignado2 = RecursoAsignado.objects.create(
            recurso=self.recurso,
            usuario=self.usuario,
            cantidad=10
        )

        self.asignado3 = RecursoAsignado.objects.create(
            recurso=self.recurso2,
            usuario=self.usuario,
            cantidad=10
        )

        self.url = reverse('recursos:transferir_recurso', kwargs={'pk': self.asignado3.id})

    def tearDown(self):
        super(TestTransferirRecursoFormView, self).tearDown()
        del self.recurso
        del self.recurso2
        del self.asignado
        del self.asignado2
        del self.asignado3
        del self.url

    def test_get_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar el acceso a la vista de crear asignación
        """

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status(self):
        """
        Autor: Daniel Lopez
        Fecha: 27 agosto 2017
        Test para probar la redirección despues de crear asignación
        """

        data = {
            'recurso': self.recurso.id,
            'usuario': self.usuario2.id,
            'cantidad': 1
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 403)

        login = self.client.login(email="usuario@usuario.co", password="prueba")
        self.assertTrue(login)

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)

        data = {
            'recurso': self.recurso2.id,
            'usuario': self.usuario2.id,
            'cantidad': 1
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)
