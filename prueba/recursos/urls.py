from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^crear/$', views.RecursoCreateView.as_view(), name='crear'),
    url(r'^editar/(?P<pk>\d+)$', views.RecursoUpdateView.as_view(), name='editar'),
    url(r'^listar$', views.RecursosListView.as_view(), name='listar'),
    url(r'^mis-recursos$', views.MisRecursosAsignadosListView.as_view(), name='mis_recursos'),
    url(r'^crear-asignacion/(?P<pk>\d+)$', views.RecursoAsignadoCreateView.as_view(), name='crear_asignacion'),
    url(r'^historial-asignaciones$', views.HistorialRecursosListView.as_view(), name='historial_asignaciones'),
    url(r'^transferir-recurso/(?P<pk>\d+)$', views.TransferirRecursoFormView.as_view(), name='transferir_recurso'),

]
