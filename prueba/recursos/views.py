from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView, ListView, UpdateView

from .models import Recurso, RecursoAsignado, HistorialRecursos
from .forms import RecursoForm, RecursoAsignadoForm, TransferenciaRecursosForm


class RecursoCreateView(LoginRequiredMixin, CreateView):
    """
    Vista para crear recursos
    """
    model = Recurso
    form_class = RecursoForm
    raise_exception = True


class RecursosListView(LoginRequiredMixin, ListView):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para ver los recursos sin asignar
    """
    model = Recurso
    raise_exception = True


class MisRecursosAsignadosListView(LoginRequiredMixin, ListView):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para ver los recursos que tengo asignados
    """
    model = RecursoAsignado
    raise_exception = True

    def get_queryset(self):
        return self.model.custom_objects.get_asignaciones_user(self.request.user)


class RecursoUpdateView(LoginRequiredMixin, UpdateView):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para editar recursos del almacen
    """
    model = Recurso
    form_class = RecursoForm
    success_msg = "Contacto editado"
    raise_exception = True


class RecursoAsignadoCreateView(LoginRequiredMixin, CreateView):
    """
    Autor: Daniel Lopez
    Fecha: 26 Agosto 2017
    Vista para la asignación de recursos a un usuario desde el almacen, se verifica que los recursos existan y
    se guarda la transaccción en el historial
    """
    model = RecursoAsignado
    raise_exception = True
    form_class = RecursoAsignadoForm
    mensaje_exito = "Se realizo la transferencia exitosamente"

    def dispatch(self, request, *args, **kwargs):
        self.recurso = Recurso.objects.get(id=self.kwargs['pk'])
        return super(RecursoAsignadoCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        recurso = form.cleaned_data['recurso']
        cantidad = form.cleaned_data['cantidad']
        usuario = form.cleaned_data['usuario']

        recurso.restar_cantidad(cantidad)

        # Se crea un registro en el historial con el cambio de recursos de el almacen al usuario
        HistorialRecursos.objects.create(recurso=form.instance.recurso,
                                         remitente=self.request.user,
                                         destinatario=form.instance.usuario,
                                         cantidad=form.instance.cantidad)

        asignacion = self.model.custom_objects.get_recurso_si_existe_por_usuario(recurso, usuario)

        # Se verifica que si el usuario tiene una asignación del mismo recurso de ser así se suma
        if asignacion:
            asignacion.sumar_cantidad_recurso(cantidad)
            messages.success(self.request, self.mensaje_exito)
            return redirect(self.get_success_url())

        return super(RecursoAsignadoCreateView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(RecursoAsignadoCreateView, self).get_form_kwargs()
        kwargs['usuario'] = self.request.user
        kwargs['recurso'] = self.recurso
        return kwargs

    def get_success_url(self):
        return reverse('recursos:historial_asignaciones')


class TransferirRecursoFormView(LoginRequiredMixin, CreateView):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para transferir recursos de un usuario a otro
    """

    model = RecursoAsignado
    form_class = TransferenciaRecursosForm
    mensaje_exito = "Se realizo la transferencia exitosamente"
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        self.asignacion = RecursoAsignado.objects.get(id=self.kwargs['pk'])
        return super(TransferirRecursoFormView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(TransferirRecursoFormView, self).get_form_kwargs()
        kwargs['usuario'] = self.request.user
        kwargs['asignacion'] = self.asignacion
        return kwargs

    def form_valid(self, form):

        recurso = form.cleaned_data['recurso']
        cantidad = form.cleaned_data['cantidad']
        usuario = form.cleaned_data['usuario']

        self.asignacion.restar_cantidad_recurso(cantidad)

        # Se crea un registro en el historial con el cambio de recursos entre usuarios
        HistorialRecursos.objects.create(recurso=self.asignacion.recurso,
                                         fuente=self.asignacion.usuario,
                                         remitente=self.request.user,
                                         destinatario=form.instance.usuario,
                                         cantidad=form.instance.cantidad)

        asignacion = self.model.custom_objects.get_recurso_si_existe_por_usuario(recurso, usuario)

        # Se verifica que si el usuario tiene una asignación del mismo recurso de ser así se suma
        if asignacion:
            asignacion.sumar_cantidad_recurso(cantidad)
            messages.success(self.request, self.mensaje_exito)
            return redirect(self.get_success_url())

        return super(TransferirRecursoFormView, self).form_valid(form)

    def get_success_url(self):
        return reverse('recursos:historial_asignaciones')


class HistorialRecursosListView(LoginRequiredMixin, ListView):
    """
    Autor: Daniel Lopez
    Fecha: 26 Agosto 2017
    Vista para ver el historial de movimientos de los recursos
    """
    model = HistorialRecursos
    raise_exception = True
