$(document).ready(function () {
    final_buttons = [
        {
            extend: 'copy',
            text: 'Copiar'
        },
        {
            extend: 'csv',
            text: 'CSV'
        },
        {
            extend: 'excel',
            text: 'Excel'
        },
        {
            extend: 'pdf',
            text: 'PDF'
        },

        {
            extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            },
            text: 'Imprimir'
        }
    ];

    $('.dataTable').dataTable({
        "order": [[ 0, "desc" ]],
        "responsive": true,
        dom: '<"html5buttons"B>lTfgitp',
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        buttons: final_buttons
    })
});