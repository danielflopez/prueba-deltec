from django import forms

from .models import PerfilUsuario


class CrearPerfilUsuarioForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CrearPerfilUsuarioForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()

    class Meta:
        model = PerfilUsuario
        fields = ('foto', 'first_name', 'last_name', 'email', 'password')


class ActualizarPerfilUsuarioForm(forms.ModelForm):

    class Meta:
        model = PerfilUsuario
        fields = ('foto',  'first_name', 'last_name', 'email')
