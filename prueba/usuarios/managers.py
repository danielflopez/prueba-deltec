from django.contrib.auth.models import UserManager


class PerfilUsuarioManager(UserManager):
    """
    Autor: Daniel Lopez
    Fecha: 26 Agosto 2017
    Manager para controlar el modelo de usuarios que se creo, esto es requerido por AbstractBaseUser de django
    """

    def create_user(self, email, password=None, **kwargs):
        """
        Autor: Daniel Lopez
        Fecha: 26 Agosto 2017
        Método para crear un usuario
        :param email: email del usuario
        :param password: contraseña (si se requiere)
        :param kwargs: parametros adicionales
        :return: usuario creado
        """
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def get_demas_usuarios(self, usuario):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para obtener todos los usuarios excepto el seleccionado
        :param usuario: usuario que sera excluido
        :return: QuerySet filtrado
        """
        return self.exclude(id=usuario.id)
