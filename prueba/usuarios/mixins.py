from django.contrib import messages


class MessageMixin(object):  # pragma: no cover
    """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Mixin para mostrar mensajes de exito o de error dependiendo la necesidad, se utiliza el framework de mensajes
        de Django y se utiliza el plugin toastr para mostrarlo desde el template
    """

    # Mensajes por defecto
    mensaje_exito = "¡La información se ha guardo exitosamente!"
    mensaje_error = "El formulario tiene errores, por favor revise la información."

    def form_valid(self, form):
        messages.success(self.request, self.mensaje_exito)
        return super(MessageMixin, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, self.mensaje_error)
        return super(MessageMixin, self).form_invalid(form)
