from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.conf import settings
from django.urls import reverse

from usuarios.managers import PerfilUsuarioManager


class PerfilUsuario(AbstractBaseUser, PermissionsMixin):

    foto = models.ImageField(upload_to='fotos', blank=True, null=True)
    email = models.EmailField(unique=True, verbose_name='Correo')
    first_name = models.CharField(max_length=30, verbose_name='Nombres')
    last_name = models.CharField(max_length=30, verbose_name='Apellidos')
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = PerfilUsuarioManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        """
        Autor: Daniel Lopes
        Fecha: 26 Agosto 2017
        Método para obtener el nombre completo de un usuario, requerido por AbstractBaseUser
        :return: nombres y apellidos acompañados de un espacio
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Autor: Daniel Lopes
        Fecha: 26 Agosto 2017
        Método para obtener el nombre corto de un usuario, requerido por AbstractBaseUser
        :return: nombres
        """
        return self.first_name

    def guess_display_name(self):
        """
        Autor: Daniel Lopes
        Fecha: 26 Agosto 2017
        Método utilizado para mostrar el nombre de un usuario o agregar 'Tu'. Especialmente util en las templates cuando
        el usuario aún no ha especificado sus nombres y apellidos en la aplicación
        :return: nombre completo del usuario o el string 'Tu'
        """
        if self.get_full_name():
            dn = self.get_full_name()
        else:
            dn = "Tu"
        dn = dn.strip()
        return dn

    def get_img_url(self):
        """
        Autor: Daniel Lopes
        Fecha: 26 Agosto 2017
        Método que retorna la foto de perfil del sistema del usuario o un comodín en el caso de no tener foto
        :return: foto del usuario
        """
        if self.foto:
            return self.foto.url
        else:
            return settings.STATIC_URL + "img/no-user.png"

    def get_absolute_url(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para retornar una url despues de un cambio en una vista del modelo
        :return: URL
        """
        return reverse('usuarios:listar')

    def __str__(self):
        return self.guess_display_name()
