from django.test import TestCase

from usuarios.forms import CrearPerfilUsuarioForm, ActualizarPerfilUsuarioForm


class TestCrearPerfilUsuarioForm(TestCase):

    def test_blank_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos en blanco en el formulario
        """
        blank_form = CrearPerfilUsuarioForm(data={})
        self.assertFalse(blank_form.is_valid())
        errors = {
            "email": ["Este campo es obligatorio."],
            "first_name": ["Este campo es obligatorio."],
            "last_name": ["Este campo es obligatorio."],
            "password": ["Este campo es obligatorio."],
        }
        self.assertEqual(blank_form.errors, errors)

    def test_invalid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos no validos del formulario
        """
        invalid_form = CrearPerfilUsuarioForm(
            data={
                'first_name': 'nombre',
                'last_name': 'apellido',
                'email': 'pls',
                'password': 'pass'
            }
        )
        self.assertFalse(invalid_form.is_valid())
        errors = {
            "email": ["Introduzca una dirección de correo electrónico válida."],
        }
        self.assertEqual(invalid_form.errors, errors)

    def test_valid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: Abril 4 2017
        Método para probar los datos validos del formulario
        """
        valid_form = CrearPerfilUsuarioForm(
            data={
                'first_name': 'nombre',
                'last_name': 'apellido',
                'email': 'prueba@prueba.co',
                'password': 'pass'
            }
        )

        self.assertTrue(valid_form.is_valid())


class TestActualizarPerfilUsuarioForm(TestCase):

    def test_blank_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos en blanco en el formulario
        """
        blank_form = ActualizarPerfilUsuarioForm(data={})
        self.assertFalse(blank_form.is_valid())
        errors = {
            "email": ["Este campo es obligatorio."],
            "first_name": ["Este campo es obligatorio."],
            "last_name": ["Este campo es obligatorio."],
        }
        self.assertEqual(blank_form.errors, errors)

    def test_invalid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Método para probar datos no validos del formulario
        """
        invalid_form = ActualizarPerfilUsuarioForm(
            data={
                'first_name': 'nombre',
                'last_name': 'apellido',
                'email': 'pls',
            }
        )
        self.assertFalse(invalid_form.is_valid())
        errors = {
            "email": ["Introduzca una dirección de correo electrónico válida."],
        }
        self.assertEqual(invalid_form.errors, errors)

    def test_valid_data(self):
        """
        Autor: Daniel Lopez
        Fecha: Abril 4 2017
        Método para probar los datos validos del formulario
        """
        valid_form = ActualizarPerfilUsuarioForm(
            data={
                'first_name': 'nombre',
                'last_name': 'apellido',
                'email': 'prueba@prueba.co',
            }
        )

        self.assertTrue(valid_form.is_valid())
