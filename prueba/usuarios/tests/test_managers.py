from django.contrib.auth import get_user_model
from django.test import TestCase

from usuarios.models import PerfilUsuario


class TestPerfilUsuarioManager(TestCase):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    TestCase para probar el manager PerfilUsuarioManager
    """

    def setUp(self):
        self.credentials = {
            'email': 'usuario@usuario.co',
            'password': 'pass',
            'first_name': 'primero'
        }
        self.usuario1 = get_user_model().objects.create_user(**self.credentials)

        self.credentials2 = {
            'email': 'usuario2@usuario.co',
            'password': 'pass2',
            'first_name': 'segundo'
        }
        self.usuario2 = get_user_model().objects.create_user(**self.credentials2)

    def tearDown(self):
        del self.credentials
        del self.usuario1
        del self.credentials2
        del self.usuario2

    def test_create_user(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el método create_user del manager
        """
        user = PerfilUsuario.objects.create_user(
            email="prueba@prueba.com",
            password="prueba",
            first_name="prueba",
            last_name="prueba"
        )
        self.assertEqual(PerfilUsuario.objects.count(), 3)
        self.assertEqual(user, PerfilUsuario.objects.get(id=user.id))

    def test_get_demas_usuarios(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el método get_demas_usuarios del manager
        """
        query = PerfilUsuario.objects.get_demas_usuarios(self.usuario1)
        self.assertEqual(list(query), list(PerfilUsuario.objects.exclude(id=self.usuario1.id)))
        self.assertEqual(query.count(), 1)
