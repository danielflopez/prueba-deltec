from django.conf import settings
from django.test import TestCase

from usuarios.models import PerfilUsuario


class TestPerfilUsuario(TestCase):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Pruebas para el modelo de PerfilUsuario
    """

    def setUp(self):
        self.usuario = PerfilUsuario.objects.create(
            email='usuario@usuario.co',
            is_active=True,
            first_name="usuario",
            last_name="usuario"
        )

    def tearDown(self):
        del self.usuario

    def test_str_representation_usuario(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar la representación como string del PerfilUsuario
        """
        self.assertEqual(str(self.usuario), self.usuario.get_full_name())

    def test_get_full_name(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar el funcionamiento del método get_full_name
        """
        full_name = '{0} {1}'.format(self.usuario.first_name, self.usuario.last_name)
        self.assertEqual(self.usuario.get_full_name(), full_name.strip())

    def test_get_short_name(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para verificar el funcionamiento del método get_short_name
        """
        self.assertEqual(self.usuario.get_short_name(), self.usuario.first_name)

    def test_get_img_url(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el método get_img_url
        """
        self.usuario.foto = True
        self.assertRaises(AttributeError, self.usuario.get_img_url)

        self.usuario.foto = None
        self.assertEqual(self.usuario.get_img_url(), settings.STATIC_URL + "img/no-user.png")

    def test_guess_display_name(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el método guess_display_name
        """

        self.assertEqual(str(self.usuario), "usuario usuario")

        self.usuario.last_name = ""
        self.usuario.first_name = ""
        self.usuario.save()

        self.assertEqual(str(self.usuario), "Tu")
