from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase


class TestCrearPerfilUsuario(TestCase):

    def setUp(self):
        self.url = reverse('usuarios:crear')

    def tearDown(self):
        del self.url

    def test_get_status_200(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el acceso a la vista de crear usuario
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status_302(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar la redireccion despues de crear un usuario
        """
        data = {'first_name': 'prueba', 'last_name': 'prueba', 'password': 'pass', 'email': 'prueba@prueba.co'}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


class TestEditarPerfilUsuarioView(TestCase):

    def setUp(self):
        self.credentials = {
            'email': 'usuario@usuario.co',
            'password': 'pass',
        }
        self.user = get_user_model().objects.create_user(**self.credentials)

        self.url = reverse('usuarios:editar', kwargs={'pk': self.user.id})

    def tearDown(self):
        del self.credentials
        del self.user
        del self.url

    def test_get_status_200(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el acceso a la vista de editar usuario
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_post_status_302(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar la redireccion despues de editar un usuario
        """
        data = {'first_name': 'prueba', 'last_name': 'prueba'}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)


class TestListaPerfilUsuariosView(TestCase):

    def setUp(self):
        self.url = reverse('usuarios:listar')

    def tearDown(self):
        del self.url

    def test_get_status_200(self):
        """
        Autor: Daniel Lopez
        Fecha: 26 agosto 2017
        Test para probar el acceso a la vista de editar usuario
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
