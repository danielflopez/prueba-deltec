from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^crear/', views.PerfilUsuarioCreateView.as_view(), name='crear'),
    url(r'^editar/(?P<pk>\d+)', views.PerfilUsuarioUpdateView.as_view(), name='editar'),
    url(r'^listar', views.PerfilUsuarioListView.as_view(), name='listar'),
]
