from django.views.generic import CreateView, ListView, UpdateView

from usuarios.mixins import MessageMixin
from .models import PerfilUsuario
from .forms import CrearPerfilUsuarioForm, ActualizarPerfilUsuarioForm


class PerfilUsuarioCreateView(CreateView, MessageMixin):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para la creación de usuarios
    """
    model = PerfilUsuario
    form_class = CrearPerfilUsuarioForm
    mensaje_exito = "Usuario creado exitosamente"

    def form_valid(self, form):
        formulario = form.save(commit=False)
        formulario.set_password(formulario.password)
        form.save()
        return super(PerfilUsuarioCreateView, self).form_valid(form)


class PerfilUsuarioListView(ListView):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para listar de usuarios
    """
    model = PerfilUsuario


class PerfilUsuarioUpdateView(UpdateView, MessageMixin):
    """
    Autor: Daniel Lopez
    Fecha: 26 agosto 2017
    Vista para la edición de usuarios
    """
    model = PerfilUsuario
    form_class = ActualizarPerfilUsuarioForm
    mensaje_exito = "Usuario editado exitosamente"
