Django==1.10.7
coverage==4.3.4
django-bootstrap3==8.2.1
flake8==3.3.0
flake8-junit-report==2.1.0
Pillow==4.0.0
django-menu-generator==1.0.2
django-cors-headers==2.0.2
django-datetime-widget==0.9.3
psycopg2==2.6.2


